#!/usr/bin/env python
import os
os.chdir('/home/mgaber/Workbench/thinkstats/work')

import inspect
import pandas as pd
import numpy as np

import thinkstats2
import thinkplot
import nsfg

# Load the data
# default function assumes files are on the same path, need absolute paths
data_path = '/home/mgaber/Workbench/thinkstats/ThinkStats2/code'
outcome_label = ['Live Birth', 'Induced Abortion', 'StillBirth', 'Miscarriage', 'Ectopic pregnancy', 'Current pregnancy']

## Probability Mass Functions
preg = nsfg.ReadFemPreg(dct_file=f'{data_path}/2002FemPreg.dct', dat_file=f'{data_path}//2002FemPreg.dat.gz')

#
firsts = preg[(preg.pregordr == 1)  & (preg.outcome == 1)]
others = preg[(preg.pregordr != 1)  & (preg.outcome == 1)]

fhist = thinkstats2.Hist(firsts.prglngth)
ohist = thinkstats2.Hist(others.prglngth)
first_pmf = thinkstats2.Pmf(fhist)
other_pmf = thinkstats2.Pmf(ohist)

# plotting
width = 0.5
thinkplot.PrePlot(2, cols=2)
thinkplot.Hist(first_pmf, align='right', width=width, label='first')
thinkplot.Hist(other_pmf, align='left', width=width, label='others')
thinkplot.Config(xlabel='weeks',
ylabel='probability',
axis=[27, 46, 0, 0.6])
thinkplot.PrePlot(2)
thinkplot.SubPlot(2)
thinkplot.Pmfs([first_pmf, other_pmf])
thinkplot.Show(xlabel='weeks',
axis=[27, 46, 0, 0.6])

fhist
x = 0
n = fhist.Total()
for i  in fhist:
    print(i, fhist[i])
    print(i, fhist[i]/len(fhist))
    x += fhist[i]/n
print(x)

## class size paradox
d = { 7: 8, 12: 8, 17: 14, 22: 4, 27: 6, 32: 12, 37: 8, 42: 3, 47: 2 }
pmf = thinkstats2.Pmf(d, label='actual')
print('Actual distrubution mean: ', pmf.Mean())

def BiasPmf(pmf, label):
    new_pmf = pmf.Copy(label=label)

    for x, p in pmf.Items():
        new_pmf.Mult(x,x)
    new_pmf.Normalize()
    return new_pmf

biased_pmf = BiasPmf(pmf, label='observed')

# plotting observed
thinkplot.PrePlot(2)
thinkplot.Pmfs([pmf, biased_pmf])
thinkplot.Show(xlabel='class size', ylabel='PMF')
