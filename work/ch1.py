#!/usr/bin/env python
import os
os.chdir('/home/mgaber/Workbench/thinkstats/work')

import inspect
import pandas as pd
import numpy as np

import thinkstats2
import thinkplot
import nsfg

# Load the data
# default function assumes files are on the same path, need absolute paths
data_path = '/home/mgaber/Workbench/thinkstats/ThinkStats2/code'
outcome_label = ['Live Birth', 'Induced Abortion', 'StillBirth', 'Miscarriage', 'Ectopic pregnancy', 'Current pregnancy']
preg = nsfg.ReadFemPreg(dct_file='/home/mgaber/Workbench/thinkstats/ThinkStats2/code/2002FemPreg.dct', dat_file='/home/mgaber/Workbench/thinkstats/ThinkStats2/code/2002FemPreg.dat.gz')

preg.head()
type(preg.columns[1])
preg.dtypes[0]
preg.outcome.value_counts().sort_index()

# Make a dictionary that maps from each respondent's `caseid` to a list of indices into the pregnancy `DataFrame`.  Use it to select the pregnancy outcomes for a single respondent.
preg_map = nsfg.MakePregMap(preg)
help(nsfg.MakePregMap)
print(inspect.getsource(nsfg.MakePregMap))

indices = preg_map[10229]
preg.outcome[indices].values

## Exercies:
# 1, count values in birthord
preg['birthord'].value_counts(dropna=False, sort=True).sort_index()
# 2, count values in prglength
preg.prglngth.value_counts(dropna=False).sort_index()
preg.prglngth
pd.cut(preg.prglngth.replace(np.nan, 0).sort_index(), [0, 13, 26, 50], right= True).value_counts(dropna=False).sort_index()

# totwgt_kg
preg.columns
preg['totalwgt_kg'] = 0.4535924 * preg['totalwgt_lb']
preg['totalwgt_kg'].mean()

# Female respondents
resp = nsfg.ReadFemResp(dct_file=f'{data_path}/2002FemResp.dct', dat_file=f'{data_path}/2002FemResp.dat.gz')

# Select the `age_r` column from `resp` and print the value counts.
resp['age_r'].value_counts().sort_index()
# youngest, oldest
resp['age_r'].value_counts().index[[0,-1]]

# caseid 2298
resp[resp.caseid == 2298]

preg[preg.caseid == 2298].outcome
# resp age
resp[resp.caseid==1].age_r
# resp perg length
preg[preg.caseid==2298].prglngth
# birthwgt_lb
preg[preg.caseid==5012].birthwgt_lb
preg[preg.caseid==5012].totalwgt_lb

# preg num
preg.pregnum.value_counts().sort_index()
preg[preg.pregnum == 19]
resp[resp.caseid==9786]

preg[preg.caseid==9786].count()

# Question: Do first babies arrive late ?
preg[(preg.pregordr == 1)  & (preg.outcome == 1)]['prglngth'].mean()
preg[(preg.pregordr != 1) & (preg.outcome == 1)]['prglngth'].mean()

preg[(preg.pregordr == 1)  & (preg.outcome == 1)]['prglngth'].describe()
preg[(preg.pregordr != 1)  & (preg.outcome == 1)]['prglngth'].describe()
